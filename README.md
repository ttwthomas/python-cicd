```
docker run -ti -p 8000:8000 -v ${PWD}:/usr/src/app -w /usr/src/app --env-file .env python bash
pip install -r requirements.txt
./manage.py migrate
python manage.py createsuperuser --noinput
python manage.py runserver 0.0.0.0:8000
```

```
./manage.py collectstatic
./manage.py test polls.tests
```